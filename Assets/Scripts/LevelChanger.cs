﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour
{
    public Animator animator;

    private int levelid;

    // Update is called once per frame
    private void Start()
    {
        
    }
    void Update()
    {
        
    }
    public void Fadetolvl(int lvlindex)
    {
        levelid = lvlindex;
        animator.SetTrigger("FadeOut");
    }
    public void EndFade()
    {
        SceneManager.LoadScene(GameInfo.LevelInfo);
    }
    public void Fading()
    {
        StartCoroutine(FadeMuerte());
    }
    IEnumerator FadeMuerte()
    {
        animator.SetBool("Fadeing", true);
        yield return new WaitForSeconds(1);
        animator.SetBool("Fadeing", false);
    }
}
