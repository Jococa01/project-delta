﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class End : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        FindObjectOfType<Audiomanager>().Stop("Footstep");
        StartCoroutine(Final());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator Final()
    {
        yield return new WaitForSeconds(3);
        Application.Quit();
    }
}
