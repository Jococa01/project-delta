﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BigDoor : MonoBehaviour
{
    public GameObject PopUp,popup2, Player;
    public int lvlid, cardNumber;
    public bool Interacting = false, ending=false;
    // Start is called before the first frame update
    void Start()
    {
        PopUp.SetActive(false);
        popup2.SetActive(false);
        cardNumber = MouseMovement.CardN;
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(transform.position, Player.transform.position);
        if (distance <= 2&&Interacting==false)
        {
            PopUp.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E)&&ending==false)
            {
                Interacting = true;
                StartCoroutine(Wrong());
            }
            if (Input.GetKeyDown(KeyCode.E)&&cardNumber>=3)
            {
                ending = true;
                GameInfo.LevelInfo = lvlid;
                FindObjectOfType<Audiomanager>().Play("Open");
                SceneManager.LoadScene(lvlid);
            }
        }
        else
        {
            PopUp.SetActive(false);
        }
    }
    IEnumerator Wrong()
    {
        PopUp.SetActive(false);
        popup2.SetActive(true);
        yield return new WaitForSeconds(2);
        popup2.SetActive(false);
        PopUp.SetActive(true);

        Interacting = false;

    }
}
