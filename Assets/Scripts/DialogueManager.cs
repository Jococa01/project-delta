﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public static bool DialogueEnd = false;
    public static float SentenceNumber = 0;
    private Queue<string> sentences;
    public Text NameText;
    public Text dialoguetxt;

    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        sentences = new Queue<string>();
    }
    public void StartDialogue(Dialogue dialogue)
    {
        animator.SetBool("IsOpen", true);
        NameText.text = dialogue.Name;

        sentences.Clear();

        foreach(string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }
    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();
            Debug.Log(SentenceNumber);
            DialogueEnd = true;
            return; 
        }
        SentenceNumber++;
        string Sentence = sentences.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(Sentence));
    }

    IEnumerator TypeSentence(string sentence)
    {
        dialoguetxt.text = "";
        foreach(char letter in sentence.ToCharArray())
        {
            dialoguetxt.text += letter;
            yield return null;
        }
    }

    void EndDialogue()
    {
        animator.SetBool("IsOpen", false);
    }
}
