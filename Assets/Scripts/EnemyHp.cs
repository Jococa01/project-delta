﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHp : MonoBehaviour
{
    public GameObject Enemy, hp1,hp2,hp3, Player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
         var Metin = Enemy.GetComponent<Enemy>().HP;
        if (Metin == 2)
        {
            hp3.SetActive(false);
        }
        if (Metin == 1)
        {
            hp2.SetActive(false);
        }
        if (Metin == 0)
        {
            hp1.SetActive(false);
        }
        transform.LookAt(Player.transform);
    }
}
