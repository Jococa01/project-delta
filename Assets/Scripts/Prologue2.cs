﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Prologue2 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        FindObjectOfType<Audiomanager>().Stop("PrologueTheme");
    }
    public void GameStart()
    {
        if (DialogueManager.DialogueEnd == true)
        {
            Debug.Log("GameStart at " + DialogueManager.SentenceNumber);
            DialogueManager.SentenceNumber = 0;
            DialogueManager.DialogueEnd = false;
            GameInfo.LevelInfo = 3;
            SceneManager.LoadScene(GameInfo.LevelInfo);
        }
    }
    public void NextPage()
    {
        FindObjectOfType<Audiomanager>().Play("Next");
    }
}
