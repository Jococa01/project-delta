﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyCounter : MonoBehaviour
{
    public GameObject Player;
    public static bool YKey = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float Playerdistance = Vector3.Distance(transform.position, Player.transform.position);
        if (Playerdistance <= 1)
        {
            YKey = true;
            MouseMovement.CardN++;
            FindObjectOfType<Audiomanager>().Play("Card");
            Debug.Log(MouseMovement.CardN);
            Destroy(gameObject);
        }

        transform.position = new Vector3(transform.position.x,0.2f*Mathf.Sin(2 * Time.time)+0.5f,transform.position.z);
        if (YKey == true)
        {
            Destroy(gameObject);
        }
    }
}
