﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMov : MonoBehaviour
{
    public Transform PlayerPos, CameraPos;
    private Vector3 CameraOffset, CameraIni;

    [Range(0.01f,1f)]
    public float Smooth = 0.5f, Despl=0.3f;


    float edge=10f;

    public bool Mirar=false;
    // Start is called before the first frame update
    void Start()
    {
        CameraIni = CameraPos.position;
        CameraOffset = transform.position - PlayerPos.position;
    }

    // Update is called once per frame
    void Update()
    {
        Mirar = false;
        if (Input.GetKey(KeyCode.LeftShift))
        {
            Mirar = true;
        }
        if (Mirar == false)
        {
            Vector3 NewPos = PlayerPos.position + CameraOffset;

            transform.position = Vector3.Slerp(transform.position, NewPos, Smooth);
        }
        if (Mirar == true)
        {
            if (Input.mousePosition.x > Screen.width - edge)
            {
                transform.position = new Vector3(transform.position.x + Despl, transform.position.y, transform.position.z);
            }
            if (Input.mousePosition.x < edge)
            {
                transform.position = new Vector3(transform.position.x - Despl, transform.position.y, transform.position.z);
            }
            if (Input.mousePosition.y > Screen.height - edge)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + Despl);
            }
            if (Input.mousePosition.y < edge)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - Despl);
            }
        }

    }
}
