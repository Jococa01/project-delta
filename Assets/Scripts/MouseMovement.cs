﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class MouseMovement : MonoBehaviour
{
    public SkinnedMeshRenderer pielecita;
    private Animator MyAnimator;
    private NavMeshAgent MyNavMesh;
    private bool Running = false, Inrange=false, canshoot=true;
    public float Attackdist = 12f, speed;
    public GameObject[] enemigos;
    public Vector3 ElBeti, target;
    public GameObject waypoint, projectile, attackable, attackunable,deadui;
    public bool AbleToShoot=false, Moving;
    public static int CardN = 0;

    public static int PlayerHP = 1;
    //public GameObject knife;
    // Start is called before the first frame update
    void Start()
    {
        PauseMenu.IsPaused = false;
        deadui.SetActive(false);
        StartCoroutine(Maintheme());
        attackunable.SetActive(false);
        MyAnimator = GetComponent<Animator>();
        MyNavMesh = GetComponent<NavMeshAgent>();

        enemigos = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject Enemigo in enemigos)
        {
            ElBeti = Enemigo.transform.position;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerHP < 1)
        {
            foreach(GameObject Enemigo in enemigos)
            {
                Destroy(Enemigo);
            }
        }


        // Movimiento del personaje con click derecho

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        if (Input.GetMouseButtonDown(1)&&GameInfo.IsAlive==true)
        {
            if (Physics.Raycast(ray, out hit, 300))
            {
                MyNavMesh.destination = hit.point;
                //Muestro visualmente el punto de destino del personaje
                Vector3 Wayposi = new Vector3(hit.point.x, hit.point.y+0.1f, hit.point.z);
                GameObject InstaWayP = Instantiate(waypoint, Wayposi,Quaternion.Euler(90,0,0)); 
                InstaWayP.SetActive(true);
                Destroy(InstaWayP, 0.5f);
            }
        }

        /*Compruebo si uno de los objetos cercanos tiene la tag de Enemigo, de ser así compruebo la distancia a la que me encuentro,
        si la distancia es menor a 15 tengo permitido atacar*/

        //Compruebo el input del mouse izq., al presionarlo obtengo información de donde he cliqueado, si lo que he cliqueado corresponde a un enemigo
        //y si tengo permitido atacar, le atacaré.


        
            if (Input.GetMouseButtonDown(0)&&GameInfo.IsAlive==true&&GameInfo.IsPaused==false)
            {
                if (Physics.Raycast(ray, out hit) && canshoot==true)
                {
                    /*
                        Primera Prueba de proyectil:
                    
                    GameObject InstKnife = Instantiate(knife, transform.position, Quaternion.Euler(new Vector3(90,0, 0))) as GameObject;
                    ProjectileManager  InstKnifescript = InstKnife.GetComponent<ProjectileManager>();
                    InstKnifescript.target = hit.point;
                    InstKnife.transform.LookAt(target);

                    */

                    //Segunda prueba de proyectil:    hit.transform.gameObject.GetComponent<Enemy>().HP--;
                    attackable.SetActive(false);
                    attackunable.SetActive(true);
                    transform.LookAt(hit.point);
                    FindObjectOfType<Audiomanager>().Play("Throw");
                    Vector3 ProjPos = new Vector3(transform.position.x, transform.position.y + 1, transform.position.z);
                    GameObject KnifeShot = Instantiate(projectile, ProjPos,transform.rotation) as GameObject;
                    KnifeShot.transform.LookAt(ProjPos,hit.point);
                    Rigidbody KnifeRigy=KnifeShot.GetComponent<Rigidbody>();
                    KnifeRigy.velocity = transform.forward * 20;
                    KnifeShot.SetActive(true);
                    Debug.Log("Disparo");
                    canshoot = false;
                    StartCoroutine(Recarga());

                }
            }

      

        //Con esto último le asigno las animaciones al personaje, en este caso detecto si está en movimiento y le pongo idle o andando.
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            FindObjectOfType<Audiomanager>().Stop("Footstep");
        }


        if (MyNavMesh.remainingDistance <= MyNavMesh.stoppingDistance&&PlayerHP>0)
        {
            Running = false;
            FindObjectOfType<Audiomanager>().Play("Footstep");
        }
        if(MyNavMesh.remainingDistance > MyNavMesh.stoppingDistance && PlayerHP > 0)
        {
            Running = true;
        }
        MyAnimator.SetBool("IsRunning", Running);
        if (PlayerHP == 0)
        {
            Muerte();
            MyAnimator.SetBool("IsRunning", false);
            FindObjectOfType<Audiomanager>().Stop("Footstep");
            StartCoroutine(CMuere());
        }

        if (Input.GetKeyDown(KeyCode.R)&&PlayerHP<=0)
        {
            Debug.Log(GameInfo.LevelInfo);
            Debug.Log("revive");
            PlayerHP = 1;
            GameInfo.IsAlive=true;
            FindObjectOfType<LevelChanger>().Fadetolvl(GameInfo.LevelInfo);
            pielecita.enabled = true;
        }
    }
    public void Muerte()
    {
        Debug.Log("moriste");
        GameInfo.IsAlive = false;
        FindObjectOfType<Audiomanager>().Play("PlayerDeath");
    }
    IEnumerator Recarga()
    {
        yield return new WaitForSeconds(1f);
        attackunable.SetActive(false);
        canshoot = true;
        attackable.SetActive(true);
        Debug.Log("recarga");
    }
    IEnumerator Maintheme()
    {
        yield return new WaitForSeconds(0.1f);
        FindObjectOfType<Audiomanager>().Play("MenuTheme");
    }
    IEnumerator CMuere()
    {
        FindObjectOfType<LevelChanger>().Fading();
        yield return new WaitForSeconds(1);
        deadui.SetActive(true);
        pielecita.enabled = false;
    }
}
