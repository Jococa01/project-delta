﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Options : MonoBehaviour
{
    public static bool Isfullscreen = true;
    public Dropdown ResSettings;
    Resolution[] resolutions;
    // Start is called before the first frame update
    void Start()
    {
        resolutions=Screen.resolutions;

        ResSettings.ClearOptions();

        List<string> options = new List<string>();

        int currentresindex = 0;
        for(int i =0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + "x" + resolutions[i].height;
            options.Add(option);

            if (resolutions[i].width == Screen.currentResolution.width&& resolutions[i].height == Screen.currentResolution.height)
            {
                currentresindex = i;
            }
        }

        ResSettings.AddOptions(options);
        ResSettings.value = currentresindex;
        ResSettings.RefreshShownValue();
    }

    // Update is called once per frame
    void Update()
    {
        Screen.fullScreen = Isfullscreen;
    }
    public void setres(int resindex)
    {
        Resolution resolution = resolutions[resindex];
        Screen.SetResolution(resolution.width, resolution.height,Screen.fullScreen);
    }

    public void ScreenMode(int isfull)
    {
        if (isfull == 0)
        {
            Isfullscreen = true;
            Debug.Log("Valor2 1");
        }
        if (isfull == 1)
        {
            Isfullscreen = false;
            Debug.Log("Valor2 2");
        }
    }
}
