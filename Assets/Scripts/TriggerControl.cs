﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TriggerControl : MonoBehaviour
{
    public GameObject PopUp, Player;
    public int lvlid;
    // Start is called before the first frame update
    void Start()
    {
        PopUp.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(transform.position, Player.transform.position);
        if (distance <= 2)
        {
            PopUp.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                GameInfo.LevelInfo = lvlid;
                FindObjectOfType<Audiomanager>().Play("Open");
                SceneManager.LoadScene(lvlid);
            }
        }
        else{
            PopUp.SetActive(false);
        }
    }
}
