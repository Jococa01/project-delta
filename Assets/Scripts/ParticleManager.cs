﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SelfDestruction());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator SelfDestruction()
    {
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }
}
