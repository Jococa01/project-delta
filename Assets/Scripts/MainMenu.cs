﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public static bool Endlvl = false;
    // Start is called before the first frame update
    void Start()
    {
        FindObjectOfType<Audiomanager>().Stop("Footstep");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void start()
    {
        SceneManager.LoadScene(GameInfo.LevelInfo);
    }
    public void Exit()
    {
        Application.Quit();
    }
}
