﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool IsPaused = false;
    public GameObject Menu;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)&&MouseMovement.PlayerHP>0)
        {
            if (IsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }
    public void Pause()
    {
        Menu.SetActive(true);
        GameInfo.IsPaused = true;
        Time.timeScale = 0f;
        IsPaused = true;
    }
    public void Resume()
    {
        Menu.SetActive(false);
        GameInfo.IsPaused = false;
        Time.timeScale = 1f;
        IsPaused = false;
    }
    public void Exit()
    {
        FindObjectOfType<Audiomanager>().Stop("Footstep");
        SceneManager.LoadScene(0);
        Time.timeScale = 1f;
        IsPaused = false;
    }
}
