﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public Transform Player;
    public NavMeshAgent Agent;
    public bool Inrange = false;
    public float Attackdist = 15f, HP=3;
    public Animator animator;
    public bool Killed=false;
    public GameObject particle;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetBool("InRange", false);
        float distance = Vector3.Distance(Agent.transform.position, Player.transform.position);
        if (Inrange == true&&Killed==false)
        {
            animator.SetBool("IsRunning", true);
            Agent.destination = Player.position;
        }
        if (distance<Attackdist)
        {
            Inrange = true;
        }
        if (distance > Attackdist)
        {
            Inrange = false;
            animator.SetBool("IsRunning", false);
        }
        if (HP == 0)
        {
            Muerte();
        }
        if (Killed == true)
        {
            animator.SetBool("IsRunning", false);
        }



        float Playerdistance = Vector3.Distance(transform.position, Player.transform.position);
        if (Playerdistance <= 1&&Killed==false&&MouseMovement.PlayerHP>0)
        {
            MouseMovement.PlayerHP--;
            StartCoroutine(Attacksequence());
        }
    }
    void Muerte()
    {
        StartCoroutine(Parti());
        Destroy(gameObject);
    }
    IEnumerator Attacksequence()
    {
        animator.SetBool("InRange", true);
        yield return new WaitForSeconds(1f);
        Killed = true;
        animator.SetBool("InRange", false);
    }
    IEnumerator Parti()
    {
        Vector3 self = new Vector3(transform.position.x, transform.position.y+1, transform.position.z);
        GameObject ClonePart = Instantiate(particle, self, Quaternion.identity) as GameObject;
        ClonePart.SetActive(true);
        FindObjectOfType<Audiomanager>().Play("Death");
        yield return new WaitForSeconds(1);
    }
}
