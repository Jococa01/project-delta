﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PrologueStart : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        FindObjectOfType<Audiomanager>().Stop("MenuTheme");

        if (DialogueManager.SentenceNumber == 10)
        {
            StartCoroutine(Equipped());
        }
        if (DialogueManager.SentenceNumber == 5)
        {
            StartCoroutine(Robots());
        }
        if (DialogueManager.SentenceNumber > 6)
        {
            FindObjectOfType<Audiomanager>().Stop("Alert");
        }
    }


    public void Triggerclick()
    {
        FindObjectOfType<Audiomanager>().Play("DoomBell");
    }
    public void NextTrigger()
    {
        FindObjectOfType<Audiomanager>().Play("Next");
    }
    public void StartMusic()
    {
        FindObjectOfType<Audiomanager>().Play("PrologueTheme");
    }
    public void SystemHelp()
    {
        if(DialogueManager.DialogueEnd==true)
        {
            Debug.Log("System Help at" + DialogueManager.SentenceNumber);
            DialogueManager.SentenceNumber = 0;
            DialogueManager.DialogueEnd = false;
            SceneManager.LoadScene(2);
        }
    }
    IEnumerator Equipped()
    {
        yield return new WaitForSeconds(0.5f);
        FindObjectOfType<Audiomanager>().Play("Pick");
    }
    IEnumerator Robots()
    {
        yield return new WaitForSeconds(0.1f);
        FindObjectOfType<Audiomanager>().Play("Alert");
    }
}
